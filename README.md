# Table of Contents
- [Repo links](#markdown-header-repo-links)
- [Manuals](#markdown-header-manuals)
    - [find the pots in the schematics](#markdown-header-find-the-pots-in-the-schematics)
- [Repair](#markdown-header-repair)
    - [next steps](#markdown-header-next-steps)
    - [Open the six-trak](#markdown-header-open-the-six-trak)

---e-n-d---

# Manuals
## Service Manual
### find the service manual online
- found `TN610-0` from 1984
    - `Model 610 Six-Trak`
    - `Preliminary Service Data`
- website
    - `http://dl.lojinx.com/analoghell/SequentialSixTrak-ServiceManual.pdf`
- saved as PDF here
    - `SequentialSixTrak-ServiceManual.pdf`
- TLDR
    - the manual is short
    - it is an overview of the architecture with some schematics
    - it is well-written
    - overview says the knobs connect to ADCs
### find the pots in the schematics
- the pots are all on page 14 of the PDF
- `U108` pins connect to four pots:
    - pin  5 - wiper on 10k linear pot for track volume
    - pin  4 - wiper on 10k linear pot for speed
    - pin 15 - wiper on 10k linear pot for value
    - pin 12 - wiper on 10k linear pot for tune
    - `U108` is just left of the *pitch wheel* in the schematic
- *pitch* and *mod* wheels are 100k linear pots
    - shown in lower right corner
- *master volume* is a 10k logarithmic pot
    - shown in upper right corner
    - assume this is logarithmic
    - convention seems to be to label pots as linear if they are
      linear, so if not labeled linear, it is probably logarithmic,
      especially since this is a *volume* knob

[ ] find the operator's manual
    - how was the sequence record supposed to work?

# Repair
## next steps
- [x] bring six-trak to Mink
- see photos
    - [x] clean desk to work on it
    - [x] keep six-trak at Mink, cover with bag

- [x] specify potentiometer
    - [x] desolder and measure resistance
        - resistance: `10k`
        - the three holes are lined up, did not measure spacing
        - the pot is also screwed on
        - you have to unscrew the entire PCB and flip it over to unscrew the pot
        - it might be possible to extract the pot without destroying it, but I
          simply snipped the three leads, unscrewed the pot, pulled the beast
          out, then attacked the leads one at a time
        - to clean the solder holes, first heat and remove the lead
        - suck up any large globs with a solder sucker
        - use copper braid: jam solder tip into hole
        - when the tip *sinks* in you have cleared the hole
        - pull the solder tip and braid out and what is left behind is a clean
          hole
        - do this aggressive soldering with the board standing up vertically
        - this gives access to both sides of the PCB and avoids resting the PCB
          on any of its delicate components
    - [x] measure the pot shaft
        - diameter: `6.32mm`
            - standard nominal diameter seems to be `6.35mm`
        - shaft length: `18mm`
            - standard nominal shaft length seems to be `19mm`

        - I may be stuck with a part like this:
> https://www.mouser.com/ProductDetail/Bourns/PDA241-HRT02-103B0?qs=sGAEpiMZZMtC25l1F4XBU7IVlqK8DuF3n3073AXoGNU%3d
        - in which case I solder jumpers from the pot to the PCB
        - if it fits the PCB, it is not a bad solution
- [ ] order samples to find a good replacement
- [ ] replace the one pot
- [ ] try cleaning the other two pots before replacing them
- [ ] if necessary, replace the other two pots:
    - [ ] desolder the Value pot last, just in case I do not have it repaired in
      time
## Open the six-trak
[x] open the sixtrak and see what can be fixed

- why does `value` knob respond intermittently?
    - nothing obviously wrong
- why does `track volume` knob not work at all
    - the potentiometer looks broken: the metal case separates a bit

- there are long screws through the wood side panels and short metal screws
  through the back
- to open the six-trak, only remove the two top-most long screw on both wood side
  panels
    - the other screws can stay in and will keep the wood panels attached to the
      six-trak body
    - the front panel is on a hinge in the back
    - these two top-most screws keep the front panel from swinging open on its
      hinge
- do not remove the short metal screws
    - these connect the sheet metal front panel to its hinge
    - it is a pain in the ass to put these back in
    - open the front panel using the hinge to avoid stressing the wires that
      connect the PCB to the power supply and the keyboard
- I might need to remove these screws for soldering
    - or maybe it is easier to just unscrew the entire PCB from the front panel
- knobs
    - it is safe to pull the knobs off the pot shafts
        - this takes a lot of force
        - the shaft presses against the PCB
    - the dangerous thing is to push the knobs back onto the pot shafts
        - this will push the pot away from the PCB, stressing the pot, the PCB,
          or the pot solder joints
## Quick internet search for six-trak knob repalcement
- according to this site they are just 100kOhm pots
- `https://syntaur.com/Items.php?Item=5764`
- that is wrong: the service manual shows 10k pots
- the `syntaur` replacement part is just a metal knob, so that doesn't help
- so the `syntaur` pot is the wrong shaft and the wrong value
    - in fairness to `syntaur`, they do specify this is to replace the pitch
      wheel pots which are 100k, but I don't see how the pot they show is
      helpful to replace the wheel pots
- the pots do look pretty standard except for the shaft
- the shaft is plastic with a keyed cutout for press-fitting the knob
- figure out the best match on Mouser and buy there
- not a big deal if I cannot use the original knobs

# Repo links
## Clone this repo
`git clone https://rainbots@bitbucket.org/rainbots/six-trak.git`
## Link to this repo
https://bitbucket.org/rainbots/six-trak/src/master/
